package cn.ydxiaoshuai.modules.weixin.util;

/**
 * @Description 接口地址
 * @author 小帅丶
 * @className WXAPI
 * @Date 2019/11/28-10:20
 **/
public class WXAPI {
    /** 发送模板接口地址 */
    public static String TEMPLATE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
    /** 获取 JSTICKET 接口地址 */
    public static String GET_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
    /** 获取 ACCESS_TOKEN 接口地址 */
    public static String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    /** 获取 通过code换取网页授权access_token  */
    public static String GET_OAUTH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
    /** 拉取用户信息(需scope为 snsapi_userinfo)  */
    public static String GET_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
    /** 拉取用户信息需要关注公众号才能拿到  */
    public static String GET_USERINFO_NO_OAUTH_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

    /** 获取 通过code换取网页授权access_token  小程序*/
    public final static String GETPAGEACCESSTOKENURL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=CODE&grant_type=authorization_code";
    //登录凭证校验接口地址
    public static String JSCODE2SESSION_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
}
