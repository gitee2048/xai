package cn.ydxiaoshuai.modules.logoexamine.service;

import cn.ydxiaoshuai.modules.logoexamine.entity.LogoExamineRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: LOGO自定义上传审核记录表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ILogoExamineRecordService extends IService<LogoExamineRecord> {

}
