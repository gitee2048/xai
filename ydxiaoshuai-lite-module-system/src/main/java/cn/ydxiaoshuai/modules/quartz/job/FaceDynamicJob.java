package cn.ydxiaoshuai.modules.quartz.job;

import cn.hutool.core.date.DateUtil;
import cn.ydxiaoshuai.common.api.vo.api.FaceDriverVirtualResponseBean;
import cn.ydxiaoshuai.common.constant.StatusConts;
import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.sdkpro.AipFacePro;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.conts.RedisBizzKey;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import cn.ydxiaoshuai.modules.facedynamic.service.IFaceDynamicTaskService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * @author 小帅丶
 * @className FaceDynamicJob
 * @Description 人脸动态任务
 * @Date 2021-05-13-17:06
 **/
@Slf4j
public class FaceDynamicJob implements Job {
    AipFacePro aipFacePro = BDFactory.getAipFacePro();
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IFaceDynamicTaskService faceDynamicTaskService;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        dealFaceDrive();//人脸驱动
        dealVirtualHuman();//虚拟主播
    }

    /**
     * @Author 小帅丶
     * @Description 虚拟主播
     * @Date  2021-05-13 17:52
     * @return void
     **/
    private void dealVirtualHuman() {
        if (redisUtil.hasKey(RedisBizzKey.VIRTUAL_HUMAN_TASK)) {
            long size = redisUtil.lGetListSize(RedisBizzKey.VIRTUAL_HUMAN_TASK);
            log.info("虚拟主播待查询任务:{}", size);
            if (size > 0) {
                List<Object> objects = redisUtil.lGet(RedisBizzKey.VIRTUAL_HUMAN_TASK, 0, -1);
                //防止有问题 一个一个循环插入
                for (int i = 0; i < objects.size(); i++) {
                    //任务ID
                    String taskId = objects.get(i).toString();
                    //查询接口返回的内容
                    JSONObject virtualHuman = aipFacePro.getVirtualHuman(taskId);
                    log.info("虚拟主播查询接口返回:{}", virtualHuman.toString());
                    //处理查询结果
                    dealResult(virtualHuman,taskId, RedisBizzKey.VIRTUAL_HUMAN_TASK);
                }
            }
        }
    }
    /**
     * @Author 小帅丶
     * @Description 人脸驱动
     * @Date  2021-05-13 17:52
     * @return void
     **/
    private void dealFaceDrive() {
        if (redisUtil.hasKey(RedisBizzKey.FACE_DRIVE_TASK)) {
            long size = redisUtil.lGetListSize(RedisBizzKey.FACE_DRIVE_TASK);
            log.info("人脸驱动待查询任务:{}", size);
            if (size > 0) {
                List<Object> objects = redisUtil.lGet(RedisBizzKey.FACE_DRIVE_TASK, 0, -1);
                //防止有问题 一个一个循环插入
                for (int i = 0; i < objects.size(); i++) {
                    //任务ID
                    String taskId = objects.get(i).toString();
                    //查询接口返回的内容
                    JSONObject virtualHuman = aipFacePro.getFaceDrive(taskId);
                    log.info("人脸驱动查询接口返回:{}", virtualHuman.toString());
                    //处理查询结果
                    dealResult(virtualHuman,taskId, RedisBizzKey.FACE_DRIVE_TASK);
                }
            }
        }
    }

    /**
     * @Author 小帅丶
     * @Description 根据状态更新数据
     * @Date  2021-05-13 18:20
     * @param virtualHuman - 接口返回的内容
     * @param key - redis的key
     * @param taskId - 任务ID
     * @return void
     **/
    private void dealResult(JSONObject virtualHuman, String taskId,String key) {
        FaceDriverVirtualResponseBean responseBean =  JSON.parseObject(virtualHuman.toString(), FaceDriverVirtualResponseBean.class);
        //查询
        FaceDynamicTask faceDynamicTaskDB = faceDynamicTaskService.getOneByTaskId(taskId);
        if(null!=faceDynamicTaskDB){
            log.info("开始更新{}",key);
            if(0 == responseBean.getError_code()){
                if(StatusConts.RUNNING.equals(responseBean.getResult().getStatus())){
                    faceDynamicTaskDB.setTaskStatus(StatusConts.RUNNING);
                }
                if(StatusConts.SUCCESS.equals(responseBean.getResult().getStatus())){
                    //先删除
                    redisUtil.lRemove(key, 1, taskId);
                    //更新数据
                    faceDynamicTaskDB.setTaskStatus(StatusConts.SUCCESS);
                    Date date = new Date();
                    faceDynamicTaskDB.setFinishDate(date);
                    faceDynamicTaskDB.setVideoUrl(responseBean.getResult().getVideo_url());
                    faceDynamicTaskDB.setExpireTime(DateUtil.offsetDay(date,30));
                }
                if(StatusConts.FAILED.equals(responseBean.getResult().getStatus())){
                    //先删除
                    redisUtil.lRemove(key, 1, taskId);
                    //更新数据
                    faceDynamicTaskDB.setTaskStatus(StatusConts.FAILED);
                    faceDynamicTaskDB.setLogId(String.valueOf(responseBean.getLog_id()));
                    faceDynamicTaskDB.setErrorCode(String.valueOf(responseBean.getError_code()));
                    faceDynamicTaskDB.setErrorMsg(responseBean.getError_msg());
                }
                boolean updateStatus = faceDynamicTaskService.updateById(faceDynamicTaskDB);
                log.info("更新状态{},{}",updateStatus,key);
            }
        }
    }
}
