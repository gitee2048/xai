package cn.ydxiaoshuai.common.sdkpro;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.error.AipError;
import com.baidu.aip.http.AipRequest;
import com.baidu.aip.http.EBodyFormat;
import com.baidu.aip.imageclassify.AipImageClassify;
import com.baidu.aip.util.Base64Util;
import com.baidu.aip.util.Util;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author 小帅丶
 * @className AipImageClassifyPro
 * @Description 邀测|组合接口
 * @Date 2020/7/17-10:19
 **/
public class AipImageClassifyPro extends AipImageClassify {
    /**组合接口*/
    static String IMAGERECOGNITION_COMBINATION_URL = "https://aip.baidubce.com/api/v1/solution/direct/imagerecognition/combination";
    /**驾驶行为识别*/
    static final String DRIVER_BEHAVIOR_URL="https://aip.baidubce.com/rest/2.0/image-classify/v1/driver_behavior";
    /**图像上色*/
    static final String COLOURIZE_URL ="https://aip.baidubce.com/rest/2.0/image-process/v1/colourize";
    /**清晰度增强*/
    static final String IMAGE_DEFINITION_ENHANCE_URL ="https://aip.baidubce.com/rest/2.0/image-process/v1/image_definition_enhance";
    /**色彩增强*/
    static final String COLOR_ENHANCE_URL ="https://aip.baidubce.com/rest/2.0/image-process/v1/color_enhance";

    public AipImageClassifyPro(String appId, String apiKey, String secretKey) {
        super(appId, apiKey, secretKey);
    }

    /**
     * 图像上色
     * @param image 图片byte
     * @return JSONObject
     */
    public JSONObject colourize(byte[] image) {
        AipRequest request = new AipRequest();
        preOperation(request);
        try {
            String base64Content = Base64Util.encode(image);
            request.addBody("image", base64Content);
            request.setUri(COLOURIZE_URL);
            postOperation(request);
            return requestServer(request);
        } catch (Exception e) {
            e.printStackTrace();
            return AipError.IMAGE_READ_ERROR.toJsonResult();
        }
    }

    /**
     * 清晰度增强
     * @param image 图片byte
     * @return JSONObject
     */
    public JSONObject imageDefinitionEnhance(byte[] image) {
        AipRequest request = new AipRequest();
        preOperation(request);
        try {
            request.setUri(IMAGE_DEFINITION_ENHANCE_URL);
            String base64Content = Base64Util.encode(image);
            request.addBody("image", base64Content);
            postOperation(request);
            return requestServer(request);
        } catch (Exception e) {
            e.printStackTrace();
            return AipError.IMAGE_READ_ERROR.toJsonResult();
        }
    }

    /**
     * 色彩增强
     * @param image 图片byte
     * @return JSONObject
     */
    public JSONObject colorEnhance(byte[] image) {
        AipRequest request = new AipRequest();
        preOperation(request);
        try {
            String base64Content = Base64Util.encode(image);
            request.setUri(COLOR_ENHANCE_URL);
            request.addBody("image", base64Content);
            postOperation(request);
            return requestServer(request);
        } catch (Exception e) {
            e.printStackTrace();
            return AipError.IMAGE_READ_ERROR.toJsonResult();
        }
    }

    /**
     * 驾驶行为识别
     * @param image 图片byte
     * @param options 其他参数
     * @return JSONObject
     */
    public JSONObject driverBehavior(byte[] image, HashMap<String, String> options) {
        AipRequest request = new AipRequest();
        preOperation(request);
        try {
            String base64Content = Base64Util.encode(image);
            request.addBody("image", base64Content);
            if (options != null) {
                request.addBody(options);
            }
            request.setUri(DRIVER_BEHAVIOR_URL);
            postOperation(request);
            return requestServer(request);
        } catch (Exception e) {
            e.printStackTrace();
            return AipError.IMAGE_READ_ERROR.toJsonResult();
        }
    }


    /**
     * 组合接口
     *
     * @param image - 图片信息base64
     * @return JSONObject
     */
    public JSONObject combination(String image) {
        AipRequest request = new AipRequest();
        preOperation(request);
        HashMap<String, Object> map = new HashMap<>();
        map = getMap(image);
        request.addBody(map);
        request.setUri(IMAGERECOGNITION_COMBINATION_URL);
        request.setBodyFormat(EBodyFormat.RAW_JSON);
        postOperation(request);
        return requestServer(request);
    }

    /**
     * 组合接口
     *
     * @param image - 图片信息base64
     * @return JSONObject
     */
    public String combinationStr(String image) {
        AipRequest request = new AipRequest();
        preOperation(request);
        HashMap<String, Object> map = new HashMap<>();
        map = getMap(image);
        String result = HttpUtil.post(IMAGERECOGNITION_COMBINATION_URL + "?access_token=" + accessToken, JSON.toJSONString(map));
        return result;
    }
    /**
     * 参数处理
     *
     * @param image - 图片信息base64
     * @return HashMap<String, Object>
     */
    private HashMap<String, Object> getMap(String image) {
        HashMap<String, Object> map = new HashMap<>();
        List<String> scenes = new ArrayList<>();
        scenes.add("advanced_general");
        scenes.add("object_detect");
        scenes.add("animal");
        scenes.add("plant");
        scenes.add("logo_search");
        scenes.add("ingredient");
        scenes.add("dishs");
        scenes.add("red_wine");
        scenes.add("currency");
        scenes.add("landmark");

        map.put("image", image);
        map.put("scenes",scenes);
        return map;
    }
}
