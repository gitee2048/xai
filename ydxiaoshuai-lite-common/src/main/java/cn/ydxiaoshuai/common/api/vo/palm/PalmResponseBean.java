package cn.ydxiaoshuai.common.api.vo.palm;

import cn.ydxiaoshuai.common.api.vo.api.BaseBean;
import lombok.Data;

/**
 * @Description 手相数据
 * @author 小帅丶
 * @className PalmResponseBean
 * @Date 2020/1/3-11:52
 **/
@Data
public class PalmResponseBean extends BaseBean {
    private PalmDetainBean data;
    public PalmResponseBean success(String msg,String msg_zh, PalmDetainBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public PalmResponseBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public PalmResponseBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
