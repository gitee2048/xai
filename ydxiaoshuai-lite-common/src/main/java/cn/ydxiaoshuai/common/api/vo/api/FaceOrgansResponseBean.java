package cn.ydxiaoshuai.common.api.vo.api;

import cn.ydxiaoshuai.common.api.vo.faceorgans.FaceFortuneBean;
import cn.ydxiaoshuai.common.api.vo.faceorgans.FaceMessageBean;
import cn.ydxiaoshuai.common.api.vo.faceorgans.FacePlantBean;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceOrgansResponseBean
 * @Description 五官分析响应对象
 * @Date 2020/9/18-15:12
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FaceOrgansResponseBean extends BaseBean{
    /**
     * 眉毛
     */
    private Data eyebrow;
    /**
     * 眼睛
     */
    private Data eye;
    /**
     * 鼻子
     */
    private Data nose;
    /**
     * 肤色
     */
    private Data skin_color;
    /**
     * 脸型
     */
    private Data face_shape;
    /**
     * 嘴唇
     */
    private Data lips;
    /**
     * 面相评分
     */
    private Data face;
    /**
     * 幸运植物
     */
    private FacePlantBean.TplDataBean.FaceFortuneBean faceFortune;
    /**
     * 今日运势
     */
    private FaceFortuneBean.TplDataBean.TextBean fortune;
    /**
     * 每日赠言
     */
    private FaceMessageBean.TplDataBean.FaceFortuneBean.DescBeanX faceMessage;

    @lombok.Data
    public static class Data{
        /**
         * 具体类型或分数
         */
        private String name;
        /**
         * 描述
         */
        private String desc;
    }

    public FaceOrgansResponseBean success(String msg,String msg_zh, Data eyebrow,Data eye,Data nose,Data skin_color,Data face_shape,Data lips,
                                          Data face,FacePlantBean.TplDataBean.FaceFortuneBean faceFortune,FaceFortuneBean.TplDataBean.TextBean fortune,
                                          FaceMessageBean.TplDataBean.FaceFortuneBean.DescBeanX faceMessage) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.eyebrow = eyebrow;
        this.eye = eye;
        this.nose = nose;
        this.skin_color = skin_color;
        this.face_shape = face_shape;
        this.lips = lips;
        this.face = face;
        this.faceFortune = faceFortune;
        this.fortune = fortune;
        this.faceMessage = faceMessage;
        return this;
    }
    public FaceOrgansResponseBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public FaceOrgansResponseBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
