package cn.ydxiaoshuai.common.api.vo.api;

import cn.ydxiaoshuai.common.constant.FaceConts;
import lombok.Data;

/**
 * @author 小帅丶
 * @className MergeBean
 * @Description 人脸融合基础对象
 * @Date 2020/5/29-14:17
 **/
@Data
public class MergeBaseBean {
    /**
     * @param image 图片 图片的分辨率要求在1920x1080以下
     */
    private String image;
    /**
     * @param image_type 图片类型
     */
    private FaceConts image_type;
    /**
     * @param quality_control 质量控制
     */
    private String quality_control;

    /**
     * @param face_location 指定模板图中进行人脸融合的人脸框位置 不指定时则默认使用最大的人脸
     */
    private String face_location;
}
