package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceMergeBean
 * @Description 人脸融合接口返回的内容
 * @Date 2020/5/29-16:06
 **/
@NoArgsConstructor
@Data
public class FaceMergeBean {

    private int error_code;
    private String error_msg;
    private long log_id;
    private int timestamp;
    private int cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private String merge_image;
        private String transfer_image;
    }
}
