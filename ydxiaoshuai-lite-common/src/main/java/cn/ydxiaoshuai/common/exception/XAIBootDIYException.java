package cn.ydxiaoshuai.common.exception;

import cn.ydxiaoshuai.common.api.vo.api.BaseBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author 小帅丶
 * @className XAIBootDIYException
 * @Description 自定义异常
 * @Date 2020/9/11-13:51
 **/
@Slf4j
@ControllerAdvice
@RestControllerAdvice
public class XAIBootDIYException {
    @Value(value = "${server.author}")
    private String author;

    final HttpHeaders httpHeaders= new HttpHeaders();

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> systemException(Exception e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("System error");
        System.out.println(e.getMessage());
        BaseBean bean = new BaseBean(500,"Internal Server Error","系统错误，请稍后再试",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(FileUploadBase.FileSizeLimitExceededException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.REQUEST_HEADER_FIELDS_TOO_LARGE)
    public ResponseEntity<Object> maxUploadSizeExceeded(Exception e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("Too Large");
        System.out.println(e.getMessage());
        BaseBean bean = new BaseBean(431,"Too Large","文件过大",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.REQUEST_HEADER_FIELDS_TOO_LARGE);
    }
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> methodNotSupported(NoHandlerFoundException e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("method not found : 404");
        System.out.println(e.getMessage());
        BaseBean bean = new BaseBean(404,"Not Found","资源未找到，请检查",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<Object> methodNotSupported(HttpRequestMethodNotSupportedException e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("method not support : 405");
        System.out.println(e.getMessage());
        BaseBean bean = new BaseBean(405,"Method Not Allowed","不允许的方法 或 请求方式错误",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public ResponseEntity<Object> mediaTypeNotSupported(HttpMediaTypeNotSupportedException e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("media type not support : 415");
        BaseBean bean = new BaseBean(415,"Unsupported Media Type","不支持的媒体类型",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ResponseEntity<Object> mediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException e) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.error("media type not acceptable : 406");
        BaseBean bean = new BaseBean(406,"Not Acceptable","无法接收的内容",author);
        return new ResponseEntity<Object>(bean,httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
