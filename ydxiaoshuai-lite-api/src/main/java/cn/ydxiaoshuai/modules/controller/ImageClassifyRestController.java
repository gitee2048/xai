package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.ImageClassifyResponseBean;
import cn.ydxiaoshuai.common.constant.ImageClassifyApiType;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.common.util.oConvertUtils;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 小帅丶
 * @className ImageClassifyRestController
 * @Description 图像识别
 * @Date 2020/9/24-12:35
 **/
@Slf4j
@Controller
@RequestMapping(value = "/rest/icr")
@Scope("prototype")
@Api(tags = "图像识别API")
public class ImageClassifyRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    /**
     * 图像识别
     * @param file 图片文件
     */
    @RequestMapping(value = "/detect", method = {RequestMethod.POST})
    public ResponseEntity<Object> detect(@RequestParam(value = "file") MultipartFile file) {
        ImageClassifyResponseBean bean = new ImageClassifyResponseBean();
        //接口类型
        String apiType = ServletRequestUtils.getStringParameter(request, "apiType","");
        try {
            startTime = System.currentTimeMillis();
            if(oConvertUtils.isEmpty(apiType)){
                bean.fail("not enough param","参数缺失，请检查",410101);
            } else {
                if(ImageClassifyApiType.animal.toString().equals(apiType)||ImageClassifyApiType.plant.toString().equals(apiType)||
                        ImageClassifyApiType.ingredient.toString().equals(apiType)||ImageClassifyApiType.car.toString().equals(apiType)||
                        ImageClassifyApiType.logo.toString().equals(apiType)||ImageClassifyApiType.flower.toString().equals(apiType)||
                        ImageClassifyApiType.landmark.toString().equals(apiType)||ImageClassifyApiType.redwine.toString().equals(apiType)||
                        ImageClassifyApiType.driverbehavior.toString().equals(apiType)||ImageClassifyApiType.currency.toString().equals(apiType)||
                        ImageClassifyApiType.dish.toString().equals(apiType)){
                    param = "image="+ Base64Util.encode(file.getBytes())+",apiType="+apiType;
                    bean = apiBeanUtil.dealICRDetectBean(file, apiType,request,version);
                }else{
                    bean.fail("api type error","类型错误，请检查",410104);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            errorMsg = e.getMessage();
            log.info("图像识别接口出错了{},接口类型{}",errorMsg,apiType);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("图像识别接口耗时{},接口类型{}",timeConsuming,apiType);
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.IMG_DETECT,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

    /**
     * 图像处理
     * @param file 图片文件
     */
    @RequestMapping(value = "/handle", method = {RequestMethod.POST})
    public ResponseEntity<Object> handle(@RequestParam(value = "file") MultipartFile file) {
        ImageClassifyResponseBean bean = new ImageClassifyResponseBean();
        //接口类型
        String apiType = ServletRequestUtils.getStringParameter(request, "apiType","");
        try {
            startTime = System.currentTimeMillis();
            if(oConvertUtils.isEmpty(apiType)){
                bean.fail("not enough param","参数缺失，请检查",410101);
            } else {
                if(ImageClassifyApiType.colourize.toString().equals(apiType)){
                    param = "image="+ Base64Util.encode(file.getBytes())+",apiType="+apiType;
                    bean = apiBeanUtil.dealICRHandleBean(file, apiType,request,version);
                }else{
                    bean.fail("api type error","类型错误，请检查",410104);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            errorMsg = e.getMessage();
            log.info("图像处理接口出错了{},接口类型{}",errorMsg,apiType);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("图像处理接口耗时{},接口类型{}",timeConsuming,apiType);
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.IMG_HANDLE,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
}
