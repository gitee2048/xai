package cn.ydxiaoshuai.modules.mapper;

import cn.ydxiaoshuai.modules.entity.LiteConfigRollnews;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 首页滚动公告表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface LiteConfigRollnewsMapper extends BaseMapper<LiteConfigRollnews> {

}
